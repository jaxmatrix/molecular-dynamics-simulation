'''
Author : Jai Shukla 
Date   : 21st Sept 2019

Description : 
    This code contains the structure to perform the MD simulation
    this is programmed by focusing its use in software for Molecular
    simulations. 

    This program can be used as a addon on any other software.

    Targets :
    1. Perform MD simulaiton 
    2. Create GUI based interface to visualise the particles that are 
    being simulaled 


'''



# Includes 
import modules.debug.debugger 
import modules.moleSim.moleSim.MoleSim


# Includes for modules for calculations 



'''

All the inputs shall be given in the software it self and it will
generate a file that will be used to handle the MD simulations

'''

if __name__=="__main__":

    # TODO : Continue or new project
    print(" Molecular MD simulation\n")
    print(" What would you like to do :\n")
    print(" 1. Continue Previous Simulation\n")
    print(" 2. New Simulation \n")

    # Take Input herer for the 
    operation = int(input("Your Choice [1/2] : "))
        

    # TODO : if new then create project and perform the initial configuration
    # of the file 
    projectName = str(input(" Enter the project name :"))
    baseDirectory = str(input(" Base Directory :"))
    moleSim = MoleSim(projectName,baseDirectory)
    debug = debugger(projectName,baseDirectory,1)

    if operation == 2:
        '''
            Inital configuraiton : 
                Starting structure :
                    If exist reuse 
                    Else create the inital configuration of the particle 
                    (only support nobel gases right now)
                Working Files : 
                    This file will store all the working data of the particle
                        Velocity   : This is the velocity data for the files to be simulated 
                        Trajectory : This store the track of the particle in the simulation
                        Energies : This will store the average energy of the particles in the system
                Configuration file :
                    Project Name    : Name of the project 
                    Home Direrctory : This is the place where all the data will be stored
                    Parameters : This is a dictionary that will hold all the inforamation that 
                    are used as a parameters for the simulation [JSON will be its structure]
                    
        ---Parameter files
        
        Equlibration Steps[eqstp] : Number of steps that will be used in equlibration 
        Simulations Steps[sistp]  : Total number of the steps after equlibration of the system
        Particle description[patdesc] : String that stores the information about the system
        '''
        print(" Initialization of the simulation")

        NOP = int(input(" Please enter the number of particles to be simulated [integer]:"))
        mass_partilce = int(input(" Enter the mass of particle [amu]:"))
        density = int(input(" Density [g/cm3]"))
        cutin = float(input(" Cutin Radius [A]:"))
        cutoff = float(input(" Cutoff Radius [A]:"))
        sigma = float(input(" Sigma [A]:"))
        epsilon = float(input(" Epsilon [kJ/mol]"))
        temperature = float(input(" Enter Temperature [T]:"))
        eqstp = int(input(" Equilibrium steps :"))
        sistp = int(input(" Simulation Steps :"))
        saveFreq = int(input(" Save Frequencey :"))
        duration = float(input(" Duration of simulation [fs]:"))
        initStructure = int(input(" Initilization sturture\n 1. FCC\n 2. Simple"))
        configFile = {
                    'Base Dir':baseDirectory,
                    'Project Name':projectName 
                }

        parameterFile = {
                    'total atoms':NOP,
                    'mass':mass_partilce,
                    'density':density,
                    'cutin':cutin,
                    'cutoff':cutoff,
                    'sigma':sigma,
                    'epsilon':epsilon,
                    'temperature':temperature,
                    'eqstp':eqstp,
                    'sistp':sistp,
                    'CurrentStep':0,
                    'saveFreq':saveFreq,
                    'timeStep': duration/sistp
                }

        moleSim.param.write(parameterFile)
        moleSim.config.write(configFile)
        moleSim.init.setParam(parameterFile)

        if(initStructure == 1):
            moleSim.init.FCC()
        elif(initStructure == 2):
            moleSim.init.Simple()
    
        debug(parameterFile=parameterFile,configFile=configFile)

    # TODO : if the simulation is resumed then load file and start the siimulations
    
    elif operation == 1: 

        # TODO : Retrive the configuration varibles and stated of the system from the configuration 
        # file 
        
        parameterFile = moleSim.param.loadParam(baseDirectory,projectName)



    # TODO : Ask for restart / continue -> if restart change the configuration file --> start 
    # simulation
    restart = bool(input("Do you want to restart simulation [y/n] :"))

    if restart == 'y': 
        parameterFile = moleSim.param.restartParam(parameterFile)

    # Now the configuration files and data files are ready hence we can start simulating
    

    # TODO : Simualated the code    
    molesim.simulate.MD(parameterFile)


    # TODO : Post processing the information
    print("Simulation Completed")

    #TODO : Make a graph showing the average energy over different steps 
    energyTrajectory = moleSim.postProcessing.getEnergyTraj(projectName,baseDirectory)

