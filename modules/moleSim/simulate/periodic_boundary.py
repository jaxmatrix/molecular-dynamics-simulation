import numpy as np 

def periodicBoundaryCondition(pos,cube_side,center='center'):

    if center == 'center':
        c = []
        for coordinate in atom: 
            if abs(coordinate) > cube_side/2:
                if coordinate >= 0 :
                    coordinate = coordinate - cube_side
                else : 
                    coordinate = coordinate + cube_side
            c.append(coordinate))
    else :
        print("specified style of placing the origin not yet implemented")

    return np.array(c,dtype='float32')

if __name__=="__main__":
    
    x = np.array(range(-9,9)).reshape(6,3)
    print(periodicBoundaryCondition(x,2))
