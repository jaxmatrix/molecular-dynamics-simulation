import numpy as np

# positions shall be reported in form of numpy array 
# postion with respect to minimum image convention 

def pos_IC(effect,observer,d_cubic):
    d = effect - observer

    for dr in d :
        if np.abs(dr) > d_cubic/2 :
            dr = dr - ((1.) if dr>0 else (-1.)) * d_cubic/2 
    
    return d

def potentialE(posD,cutin,cutoff,sigma,epsilon,shifted=False,shift=10):
    mag = np.sqrt(np.sum(posD**2))
    
    # Removing cutin position as this will need correction in terms of potential value
    #print(mag) 
    if mag <= cutoff:
        p = LJ(epsilon,sigma,mag)
        f = forceVector(posD,mag,epsilon,sigma);

        if shifted:
            v_shift = LJ(epsilon,sigma,shift)
            p_shifted = p - v_shift
    else:
        p = 0
        f = np.zeros(3,dtype="float64")
        p_shifted = 0
    #print(p)

    if shifted:
        return p,f,p_shifted
    else:
        return p,f


def LJ(epsilon,sigma,r):
    return 4. * epsilon * ( (sigma/r) ** 12 - (sigma/r) ** 6)

def forceVector(relPosVector,r,epsilon,sigma):
    f_magnitude = - 4. * epsilon * ( -12. / sigma * (sigma/r) ** 13. + 6. / sigma * (sigma/r) ** 7)
    
    fx = f_magnitude*relPosVector[0]/r
    fy = f_magnitude*relPosVector[1]/r
    fz = f_magnitude*relPosVector[2]/r

    return np.array([fx,fy,fz])

if __name__ == "__main__" :
    particle1 = np.array([1,2,3])
    particle2 = np.array([2,3,4])
    r = pos_IC(particle1,particle2,100)
    p,f,sh = potentialE(r,0,10,1.,1.,True)

    print(particle1,particle2,r,p,f,sh)
