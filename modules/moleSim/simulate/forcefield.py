import potential_analysis
import periodic_boundary

import math

def forceOnAtoms(forceMatrix):
    interactions = forceMatrix - forceMatrix.transpose()
    forceOnAtoms = np.sum(interactions,axis=1)

    return forceOnAtoms

'''
    mass_molecule_amu : mass of the molecule 
    pos_data_now : numpy array that contains the initial position data of the particles 
    pas_data_old : Previous position of the particle 
'''

def trajectory(mass_molecule_amu, number_atom, epsilon, sigma, cutin, cutoff, dt, lbox, pos_data_now, vel_data, shifted=False, shift=10, **kwargs):
    # pos_data = moleculeData('Data/argon_365.dat',2)
    
    total_pairwise_potential = []
    total_force = []
    
    p = np.zeros(number_atom)
    f = np.zeros((3,number_atom,number_atom),'float64')
    
    mass_argon = mass_molecule_amu # mass in angrstrom
    mass_proton = 1.673e-27 #Mass in KG

    #dt = 20  # dt is in femto seconds

    mass_molecule = mass_proton * mass_molecule_amu    
    
    for j in range(0,number_atom - 1 ):
        for k in range(j+1, number_atom):
            pot_temp,force_temp = potentialE(pos_IC(pos_data_now[j],pos_data_now[k],lbox),cutin,cutoff,sigma,epsilon)
            #if (pot_temp > 0 ) : print("Potential : " + str(j) + ":"+ str(k) + ":" + str(pot_temp))

            f[0,j,k] = force_temp[0]
            f[1,j,k] = force_temp[1]
            f[2,j,k] = force_temp[2]
            
        #print(pot_temp-pot_shift_temp)


    # Simulation configurations 


    F = np.array([forceOnAtoms(f[0]),forceOnAtoms(f[1]),forceOnAtoms(f[2])]);
    acceleration = F/mass_molecule 
        
    pos_data_new = periodicBoundaryCondition(pos_data_nos + dt*vel_data + dt**2*acceleration,cell_length)

    vel_data_new = vel_data + dt*acceleration 
    
    return pos_data_new,vel_data_new 


if __name__ == "__main__":
    
    # Start the configuration files 
    
    

    traj = np.array(moleculeData('Data/argon_365.dat',2))
    epsilon  = 1
    sigma = 3.4
    cutin = 1
    cutoff = 10
    dt = 20
    lbox = 26
    mass_molecule_amu = 40 
    number_atom = traj.shape[0]
   
    print()
    for i in range(10):
        if (i == 0):
            traj_temp = trajectory(mass_molecule_amu, number_atom, epsilon, sigma, cutin, cutoff, dt, lbox, traj[-1])
        else : 
            traj_temp = trajectory(mass_molecule_amu, number_atom, epsilon, sigma, cutin, cutoff, dt, lbox, traj[-2])

        traj.append(traj_temp)

    print(traj)

