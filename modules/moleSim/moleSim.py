import os 

import param
import data
import postProcessing
import config
from modules.init import initlize as init
from modules.simulate import simulate 

class MoleSim:
    def __init__(self,projectName,baseDirectory):
        self.path = os.path.realpath(baseDirectory)
        self.projectName = projectName 
        self.param = param(self.projectName,self.path)
        self.data = data(self.projectName,self.path)
        self.config = config(self.projectName,self.path)
        self.init = init(self.data)
        self.simulate = simulate(self.projectName,self.path,self.data)

