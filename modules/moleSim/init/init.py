import numpy as np
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt

class initlize:
    def __init__(self,data):
        self.paramLoaded = False
        self.data = data
        print(" Partilcle Inilizer Loaded") 

    def setParam(self,param):
        self.mass = param['mass']
        self.density = param['density']
        self.atoms = param['total atoms']
        self.paramLoaded = True

    def __get_unit_length(mol_mass,density,system_type='cubic'):

        z = 1
        if system_type=='cubic':
            z = 1
        elif system_type=='fcc':
            z = 2
        return (1.*mol_mass*z/(1.*density*6.022*10**23))**(1./3.)


    def Cubic(self,mol_mass,density,atoms_total):
        if(self.paramLoaded):
            
            mol_mass = self.mass 
            density = self.density 
            atoms_total = self.atoms 

            length = self.__get_unit_length(mol_mass,density)
            
            nos_atom_side = int((atoms_total)**(1./3)) + 1
            
            # TODO : account for the size of the atom 

            atom_coordinate = []
            x = 0
            y = 0
            z = 0
            atom_index = 0
            while (atom_index < atoms_total):


                
                atom_coordinate.append([x,y,z])
                atom_index = atom_index + 1

                x = x+length

                if(not atom_index % nos_atom_side):
                    y = y + length
                    x = 0

                if(not atom_index % nos_atom_side**2):
                    z = z + length
                    y = 0

            self.data.write(atom_coordinate,projectName + '_pos_' + '0','list')        
            
            return atom_coordinate
        else :
            print("Please Load the parameter file")

    def FCC(self):
        if(self.paramLoaded):
            mol_mass = self.mass
            density = self.density
            atoms_total = self.atoms 
            
            length = self__get_unit_length(mol_mass,density,'fcc')

            nos_atom_side = int((atoms_total/4)**(1./3)) + 1 
            print (nos_atom_side,length)
            atom_coordinate = []
            x_index = 0
            y_index = 0 
            z_index = 0
            atom_index = 0
            c = [0,0,0]

            x = 0
            y = 0
            z = 0
            while( atom_index < atoms_total):

               #print(x_index,y_index,z_index)
               x = x_index*length*0.5 
               y = y_index*length*0.5
               z = z_index*length*0.5

               atom_coordinate.append([x,y,z])
               atom_index = atom_index + 1

               x_index = x_index + 2

               if( x_index >= 2*nos_atom_side ):
                   y_index = y_index + 1
                   x_index = (x_index + 1)%2 
               if( y_index >= 2*nos_atom_side):
                    y_index = 0
                    x_index = 0
                    z_index = z_index + 1

            self.data.write(atom_coordinate,projectName + '_pos_' + '0','list')
            return atom_coordinate
        else:
            print("Please set the parameter file")

    def initVelocity(self):
        if self.paramLoaded:
            vel = np.random.normal(size=paramFile['total atoms']*3).reshape((paramFile['total atoms'],3))
            self.data.write(vel.tolist(),projectName + '_vel_' + '0','list')        


