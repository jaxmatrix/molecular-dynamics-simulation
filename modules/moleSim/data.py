import numpy as np


'''
    Structure of data :

    {
        state : # // Frame on which calcualtion is taken care of
        position : position list,
        velocity : velocity list
    }


'''

class data:
    def __init__(self,projectName, basePath):
        self.path = basePath
        self.projectName = projectName 

    def load(self,fileName):
        simFilePath = self.path + '/data/' + fileName + '.sf'
        f = open(simFilePath,'r')
        data = f.read()
        
        data = json.loads(data)
        
        if data['type'] == 'list':
            returnData = data['data']
        else :
            returnData = data['data']
        
        return returnData 

    # Expecting it to be a list type data
    def write(self,data,fileName,dataType='list'):
        path = self.path + '/data/' + fileName + '.sf'
        f = open(path,'w')
        
        toBeWritten = { 'type':dataType, 'data':data}
        f.write(data)
        f.close()

        
